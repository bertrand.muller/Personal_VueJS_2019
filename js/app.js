new Vue({
    el: '#app',
    data: {
        message: 'Salut les gens !',
        link: 'https://graphikart.fr',
        success: true,
        cls: 'success',
        persons: ['Jonathan', 'Marion', 'Marine', 'Jean', 'Patrick']
    },
    methods: {
        close: function() {
            this.message = "Fermé";
            this.success = false;
        },
        style: function() {
            if(this.success) {
                return {background: '#00FF00'}
            } else {
                return {background: '#FF0000'}
            }
        }
    }
});